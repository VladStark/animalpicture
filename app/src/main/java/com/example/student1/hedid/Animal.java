package com.example.student1.hedid;

/*
 * Created by student1 on 10.11.16.
 */
public abstract class Animal {

    public abstract void eat ();
    public abstract void sleep();

    public abstract int getImageId();
    public abstract String getName();

    @Override
    public final String toString() {
        return "this animal object";
    }

}
