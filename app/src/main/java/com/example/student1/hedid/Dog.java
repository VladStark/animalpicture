package com.example.student1.hedid;

import android.util.Log;

/*
 * Created by student1 on 10.11.16.
 */
public class Dog extends  Animal {

    private static final String LOG_TAG = "Dog : ";

    private String name;
    private int imageId = R.drawable.panda;

    public Dog(String name) {
        this.name = name;
    }

    @Override
    public  void eat() {
        Log.d(LOG_TAG, "Dog eat");
    }
    @Override
    public void sleep () {
        Log.d(LOG_TAG, "Dog sleep");
    }
    @Override
    public String getName () {
        return name;
    }
    @Override
    public int getImageId() {
        return imageId;
    }

}
