package com.example.student1.hedid;

import android.support.v4.view.ViewGroupCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layout = (LinearLayout) findViewById(R.id.layout);

        ArrayList<Animal> list = new ArrayList<>();

        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("     TOM    "));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));
        list.add(new Dog("TOM"));


        for (Animal animal : list) {

            ImageView image = new ImageView(this);
            TextView text = new TextView(this);
            image.setLayoutParams(new ViewGroup.LayoutParams(500,500));
            image.setImageResource(animal.getImageId());
            text.setText(animal.getName());
            layout.addView(image);
            layout.addView(text);
        }

    }
}
